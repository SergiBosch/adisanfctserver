package com.adisanfct.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.AnaliticaDao;
import com.adisanfct.dao.implementation.specific.CatalogoanaliticasDao;
import com.adisanfct.dao.implementation.specific.DependenciaDao;
import com.adisanfct.dao.implementation.specific.EpisodioDao;
import com.adisanfct.dao.implementation.specific.MedicoDao;
import com.adisanfct.dao.implementation.specific.UsuarioDao;
import com.adisanfct.entity.AnaliticaEntity;
import com.adisanfct.entity.CatalogoanaliticasEntity;
import com.adisanfct.entity.DependenciaEntity;
import com.adisanfct.entity.EpisodioEntity;
import com.adisanfct.entity.MedicoEntity;
import com.adisanfct.entity.UsuarioEntity;

@Service
public class AnaliticaService{
	@Autowired
	AnaliticaDao oAnaliticaDao;
	
	@Autowired
	EpisodioDao oEpisodioDao;
	
	@Autowired
	CatalogoanaliticasDao oCatalogoanaliticasDao;
	
	@Autowired
	DependenciaDao oDependenciaDao;
	
	@Autowired
	UsuarioDao oUsuarioDao;
	
	@Autowired
	MedicoDao oMedicoDao;
	
	public AnaliticaEntity get(int id) {
		return (AnaliticaEntity) oAnaliticaDao.get(id);
	}

	public List<AnaliticaEntity> getall() {
		return oAnaliticaDao.getall();
	}

	public Long count() {
		return oAnaliticaDao.count();
	}

	public Page<AnaliticaEntity> getPage(Pageable oPageable) {
		return oAnaliticaDao.getPage(oPageable);
	}
	
	@SuppressWarnings("deprecation")
	public boolean insert(Map<String, String> mParametros) {
		AnaliticaEntity oAnaliticaEntity = new AnaliticaEntity();
		oAnaliticaEntity.setDescripcion(mParametros.get("descripcion"));
		if(mParametros.get("fecha_prevista")!="" && mParametros.get("fecha_prevista")!=null) {
			oAnaliticaEntity.setFecha_prevista(new Date(mParametros.get("fecha_prevista")));
		}
		if(mParametros.get("fecha_realizacion")!="" && mParametros.get("fecha_realizacion")!=null) {
			oAnaliticaEntity.setFecha_realizacion(new Date(mParametros.get("fecha_realizacion")));
		}
		oAnaliticaEntity.setUbicacion(mParametros.get("ubicacion"));
		oAnaliticaEntity.setImporte(Double.parseDouble(mParametros.get("importe")));
		oAnaliticaEntity.setId_catalogoanaliticas((CatalogoanaliticasEntity) oCatalogoanaliticasDao.get(Integer.parseInt(mParametros.get("id_catalogoanaliticas"))));
		oAnaliticaEntity.setId_dependencia((DependenciaEntity) oDependenciaDao.get(Integer.parseInt(mParametros.get("id_dependencia"))));
		oAnaliticaEntity.setId_medico((MedicoEntity) oMedicoDao.get(Integer.parseInt(mParametros.get("id_medico"))));
		oAnaliticaEntity.setId_episodio((EpisodioEntity) oEpisodioDao.get(Integer.parseInt(mParametros.get("id_episodio"))));
		oAnaliticaEntity.setId_usuario((UsuarioEntity) oUsuarioDao.get(Integer.parseInt(mParametros.get("id_usuario"))));
		if(oAnaliticaDao.create(oAnaliticaEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(Map<String, String> mParametros) {
		AnaliticaEntity oAnaliticaEntity = new AnaliticaEntity();
		oAnaliticaEntity.setId(Integer.parseInt(mParametros.get("id")));
		oAnaliticaEntity.setDescripcion(mParametros.get("descripcion"));
		if(mParametros.get("fecha_prevista")!="" && mParametros.get("fecha_prevista")!=null) {
			oAnaliticaEntity.setFecha_prevista(new Date(mParametros.get("fecha_prevista")));
		}
		if(mParametros.get("fecha_realizacion")!="" && mParametros.get("fecha_realizacion")!=null) {
			oAnaliticaEntity.setFecha_realizacion(new Date(mParametros.get("fecha_realizacion")));
		}
		oAnaliticaEntity.setUbicacion(mParametros.get("ubicacion"));
		oAnaliticaEntity.setImporte(Double.parseDouble(mParametros.get("importe")));
		oAnaliticaEntity.setId_catalogoanaliticas((CatalogoanaliticasEntity) oCatalogoanaliticasDao.get(Integer.parseInt(mParametros.get("id_catalogoanaliticas"))));
		oAnaliticaEntity.setId_dependencia((DependenciaEntity) oDependenciaDao.get(Integer.parseInt(mParametros.get("id_dependencia"))));
		oAnaliticaEntity.setId_medico((MedicoEntity) oMedicoDao.get(Integer.parseInt(mParametros.get("id_medico"))));
		oAnaliticaEntity.setId_episodio((EpisodioEntity) oEpisodioDao.get(Integer.parseInt(mParametros.get("id_episodio"))));
		oAnaliticaEntity.setId_usuario((UsuarioEntity) oUsuarioDao.get(Integer.parseInt(mParametros.get("id_usuario"))));
		if(oAnaliticaDao.update(oAnaliticaEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oAnaliticaDao.delete(id);
	}

}

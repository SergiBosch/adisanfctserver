package com.adisanfct.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.InstrumentalistaDao;
import com.adisanfct.entity.InstrumentalistaEntity;

@Service
public class InstrumentalistaService{
	@Autowired
	InstrumentalistaDao oInstrumentalistaDao;
	
	public 	InstrumentalistaEntity get(int id) {
		return (InstrumentalistaEntity) oInstrumentalistaDao.get(id);
	}

	@SuppressWarnings("unchecked")
	public List<InstrumentalistaEntity> getall() {
		return oInstrumentalistaDao.getall();
	}

	public List<InstrumentalistaEntity> getFilter(int centrosanitario) {
		return oInstrumentalistaDao.getFilter(centrosanitario);
	}


}

package com.adisanfct.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.MedicamentoDao;
import com.adisanfct.entity.MedicamentoEntity;

@Service
public class MedicamentoService{
	@Autowired
	MedicamentoDao oMedicamentoDao;
	
	public 	MedicamentoEntity get(int id) {
		return (MedicamentoEntity) oMedicamentoDao.get(id);
	}

	@SuppressWarnings("unchecked")
	public List<MedicamentoEntity> getall() {
		return oMedicamentoDao.getall();
	}


}

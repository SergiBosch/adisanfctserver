package com.adisanfct.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.CatalogoanaliticasDao;
import com.adisanfct.entity.CatalogoanaliticasEntity;

@Service
public class CatalogoanaliticasService{
	@Autowired
	CatalogoanaliticasDao oCatalogoanaliticasDao;
	
	public 	CatalogoanaliticasEntity get(int id) {
		return (CatalogoanaliticasEntity) oCatalogoanaliticasDao.get(id);
	}

	@SuppressWarnings("unchecked")
	public List<CatalogoanaliticasEntity> getall() {
		return oCatalogoanaliticasDao.getall();
	}


}

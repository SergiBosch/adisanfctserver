package com.adisanfct.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.EpisodioDao;
import com.adisanfct.dao.implementation.specific.PacienteDao;
import com.adisanfct.entity.EpisodioEntity;
import com.adisanfct.entity.PacienteEntity;

@Service
public class EpisodioService{
	@Autowired
	EpisodioDao oEpisodioDao;
	
	@Autowired
	PacienteDao oPacienteDao;
	
	public EpisodioEntity get(int id) {
		return (EpisodioEntity) oEpisodioDao.get(id);
	}

	public List<EpisodioEntity> getall() {
		return oEpisodioDao.getall();
	}

	public Long count() {
		return oEpisodioDao.count();
	}
	

	public Long countEpisodioPaciente(int paciente) {
		return oEpisodioDao.countEpisodioPaciente((PacienteEntity) oPacienteDao.get(paciente));
	}

	public Page<EpisodioEntity> getPage(Pageable oPageable) {
		return oEpisodioDao.getPage(oPageable);
	}
	
	public List<EpisodioEntity> getEpisodiosPaciente(int paciente, Pageable oPageable) {
		return oEpisodioDao.getEpisodiosPaciente((PacienteEntity) oPacienteDao.get(paciente), oPageable);
	}
	
	public boolean insert(Map<String, String> mParametros) {
		EpisodioEntity oUsuarioEntity = new EpisodioEntity();
		if(oEpisodioDao.create(oUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(Map<String, String> mParametros) {
		EpisodioEntity oUsuarioEntity = new EpisodioEntity();
		if(oEpisodioDao.update(oUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oEpisodioDao.delete(id);
	}


	

}

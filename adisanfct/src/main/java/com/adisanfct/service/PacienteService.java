package com.adisanfct.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.PacienteDao;
import com.adisanfct.entity.PacienteEntity;

@Service
public class PacienteService{
	@Autowired
	PacienteDao oPacienteDao;
	
	public PacienteEntity get(int id) {
		return (PacienteEntity) oPacienteDao.get(id);
	}

	public List<PacienteEntity> getall() {
		return oPacienteDao.getall();
	}

	public Long count() {
		return oPacienteDao.count();
	}

	public Page<PacienteEntity> getPage(Pageable oPageable) {
		return oPacienteDao.getPage(oPageable);
	}
	

	public Page<PacienteEntity> getPageFilter(Pageable oPageable, String filtro) {
		return oPacienteDao.getPageFilter(oPageable, filtro);
	}
	
	public boolean insert(Map<String, String> mParametros) {
		PacienteEntity oUsuarioEntity = new PacienteEntity();
		if(oPacienteDao.create(oUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(Map<String, String> mParametros) {
		PacienteEntity oUsuarioEntity = new PacienteEntity();
		if(oPacienteDao.update(oUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oPacienteDao.delete(id);
	}

	public Long countFilter(String filter) {
		return oPacienteDao.countFilter(filter);
	}


}

package com.adisanfct.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.MedicoDao;
import com.adisanfct.entity.MedicoEntity;

@Service
public class MedicoService{
	@Autowired
	MedicoDao oMedicoDao;
	
	public 	MedicoEntity get(int id) {
		return (MedicoEntity) oMedicoDao.get(id);
	}

	@SuppressWarnings("unchecked")
	public List<MedicoEntity> getall() {
		return oMedicoDao.getall();
	}

	public List<MedicoEntity> getFilter(int centrosanitario) {
		return oMedicoDao.getFilter(centrosanitario);
	}


}

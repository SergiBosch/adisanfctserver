package com.adisanfct.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.ViaDao;
import com.adisanfct.entity.ViaEntity;

@Service
public class ViaService{
	@Autowired
	ViaDao oViaDao;
	
	public 	ViaEntity get(int id) {
		return (ViaEntity) oViaDao.get(id);
	}

	@SuppressWarnings("unchecked")
	public List<ViaEntity> getall() {
		return oViaDao.getall();
	}


}

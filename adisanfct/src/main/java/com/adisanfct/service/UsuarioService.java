package com.adisanfct.service;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.UsuarioDao;
import com.adisanfct.entity.UsuarioEntity;

@Service
public class UsuarioService{
	@Autowired
	UsuarioDao oUsuarioDao;
	
	public UsuarioEntity get(int id) {
		return (UsuarioEntity) oUsuarioDao.get(id);
	}

	public List<UsuarioEntity> getall() {
		return oUsuarioDao.getall();
	}

	public Long count() {
		return oUsuarioDao.count();
	}

	public Page<UsuarioEntity> getPage(Pageable oPageable) {
		return oUsuarioDao.getPage(oPageable);
	}
	
	public boolean insert(Map<String, String> mParametros) {
		UsuarioEntity oUsuarioEntity = new UsuarioEntity();
		if(oUsuarioDao.create(oUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(Map<String, String> mParametros) {
		UsuarioEntity oUsuarioEntity = new UsuarioEntity();
		if(oUsuarioDao.update(oUsuarioEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oUsuarioDao.delete(id);
	}

	public UsuarioEntity login(Map<String, String> mParametros) {
		return oUsuarioDao.login(mParametros.get("login"), mParametros.get("password"));
	}

}

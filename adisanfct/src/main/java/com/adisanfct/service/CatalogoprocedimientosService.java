package com.adisanfct.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.CatalogoprocedimientosDao;
import com.adisanfct.entity.CatalogoprocedimientosEntity;

@Service
public class CatalogoprocedimientosService{
	@Autowired
	CatalogoprocedimientosDao oCatalogoprocedimientosDao;
	
	public 	CatalogoprocedimientosEntity get(int id) {
		return (CatalogoprocedimientosEntity) oCatalogoprocedimientosDao.get(id);
	}

	@SuppressWarnings("unchecked")
	public List<CatalogoprocedimientosEntity> getall() {
		return oCatalogoprocedimientosDao.getall();
	}


}

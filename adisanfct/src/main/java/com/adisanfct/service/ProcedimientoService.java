package com.adisanfct.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.adisanfct.dao.implementation.specific.CatalogoprocedimientosDao;
import com.adisanfct.dao.implementation.specific.DependenciaDao;
import com.adisanfct.dao.implementation.specific.EpisodioDao;
import com.adisanfct.dao.implementation.specific.InstrumentalistaDao;
import com.adisanfct.dao.implementation.specific.MedicoDao;
import com.adisanfct.dao.implementation.specific.PrioridadDao;
import com.adisanfct.dao.implementation.specific.ProcedimientoDao;
import com.adisanfct.dao.implementation.specific.UsuarioDao;
import com.adisanfct.entity.CatalogoprocedimientosEntity;
import com.adisanfct.entity.DependenciaEntity;
import com.adisanfct.entity.EpisodioEntity;
import com.adisanfct.entity.InstrumentalistaEntity;
import com.adisanfct.entity.MedicoEntity;
import com.adisanfct.entity.PrioridadEntity;
import com.adisanfct.entity.ProcedimientoEntity;
import com.adisanfct.entity.UsuarioEntity;

@Service
public class ProcedimientoService{
	@Autowired
	ProcedimientoDao oProcediminetoDao;
	
	@Autowired
	EpisodioDao oEpisodioDao;
	
	@Autowired
	PrioridadDao oPrioridadDao;
	
	@Autowired
	CatalogoprocedimientosDao oCatalogoprocedimientosDao;
	
	@Autowired
	InstrumentalistaDao oInstrumentalistaDao;
	
	@Autowired
	MedicoDao oMedicoDao;
	
	@Autowired
	DependenciaDao oDependenciaDao;
	
	@Autowired
	UsuarioDao oUsuarioDao;
	
	public ProcedimientoEntity get(int id) {
		return (ProcedimientoEntity) oProcediminetoDao.get(id);
	}

	public List<ProcedimientoEntity> getall() {
		return oProcediminetoDao.getall();
	}

	public Long count() {
		return oProcediminetoDao.count();
	}

	public Page<ProcedimientoEntity> getPage(Pageable oPageable) {
		return oProcediminetoDao.getPage(oPageable);
	}
	
	@SuppressWarnings("deprecation")
	public boolean insert(Map<String, String> mParametros) {
		ProcedimientoEntity oProcedimientoEntity = new ProcedimientoEntity();
		if(mParametros.get("fecha_inicio")!="" && mParametros.get("fecha_inicio")!=null) {
			oProcedimientoEntity.setFecha_inicio(new Date(mParametros.get("fecha_inicio")));
		}
		if(mParametros.get("fecha_fin")!="" && mParametros.get("fecha_fin")!=null) {
			oProcedimientoEntity.setFecha_fin(new Date(mParametros.get("fecha_fin")));
		}
		oProcedimientoEntity.setDiagnostico_inicial(mParametros.get("diagnostico_inicial"));
		oProcedimientoEntity.setDiagnostico_final(mParametros.get("diagnostico_final"));
		oProcedimientoEntity.setProcedimiento_previsto(mParametros.get("procedimiento_previsto"));
		oProcedimientoEntity.setProcedimiento_realizado(mParametros.get("procedimiento_realizado"));
		oProcedimientoEntity.setId_prioridad((PrioridadEntity) oPrioridadDao.get(Integer.parseInt(mParametros.get("id_prioridad"))));
		oProcedimientoEntity.setId_procedimiento((CatalogoprocedimientosEntity) oCatalogoprocedimientosDao.get(Integer.parseInt(mParametros.get("id_procedimiento"))));
		oProcedimientoEntity.setId_instrumentalista((InstrumentalistaEntity) oInstrumentalistaDao.get(Integer.parseInt(mParametros.get("id_instrumentalista"))));
		oProcedimientoEntity.setId_medico((MedicoEntity) oMedicoDao.get(Integer.parseInt(mParametros.get("id_medico"))));
		oProcedimientoEntity.setId_dependencia((DependenciaEntity) oDependenciaDao.get(Integer.parseInt(mParametros.get("id_dependencia"))));
		oProcedimientoEntity.setInforme(mParametros.get("informe"));
		oProcedimientoEntity.setId_episodio((EpisodioEntity) oEpisodioDao.get(Integer.parseInt(mParametros.get("id_episodio"))));
		oProcedimientoEntity.setId_usuario((UsuarioEntity) oUsuarioDao.get(Integer.parseInt(mParametros.get("id_usuario"))));

		if(oProcediminetoDao.create(oProcedimientoEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(Map<String, String> mParametros) {
		ProcedimientoEntity oProcedimientoEntity = new ProcedimientoEntity();
		oProcedimientoEntity.setId(Integer.parseInt(mParametros.get("id")));
		if(mParametros.get("fecha_inicio")!="" && mParametros.get("fecha_inicio")!=null) {
			oProcedimientoEntity.setFecha_inicio(new Date(mParametros.get("fecha_inicio")));
		}
		if(mParametros.get("fecha_fin")!="" && mParametros.get("fecha_fin")!=null) {
			oProcedimientoEntity.setFecha_fin(new Date(mParametros.get("fecha_fin")));
		}
		oProcedimientoEntity.setDiagnostico_inicial(mParametros.get("diagnostico_inicial"));
		oProcedimientoEntity.setDiagnostico_final(mParametros.get("diagnostico_final"));
		oProcedimientoEntity.setProcedimiento_previsto(mParametros.get("procedimiento_previsto"));
		oProcedimientoEntity.setProcedimiento_realizado(mParametros.get("procedimiento_realizado"));
		oProcedimientoEntity.setId_prioridad((PrioridadEntity) oPrioridadDao.get(Integer.parseInt(mParametros.get("id_prioridad"))));
		oProcedimientoEntity.setId_procedimiento((CatalogoprocedimientosEntity) oCatalogoprocedimientosDao.get(Integer.parseInt(mParametros.get("id_procedimiento"))));
		oProcedimientoEntity.setId_instrumentalista((InstrumentalistaEntity) oInstrumentalistaDao.get(Integer.parseInt(mParametros.get("id_instrumentalista"))));
		oProcedimientoEntity.setId_medico((MedicoEntity) oMedicoDao.get(Integer.parseInt(mParametros.get("id_medico"))));
		oProcedimientoEntity.setId_dependencia((DependenciaEntity) oDependenciaDao.get(Integer.parseInt(mParametros.get("id_dependencia"))));
		oProcedimientoEntity.setInforme(mParametros.get("informe"));
		oProcedimientoEntity.setId_episodio((EpisodioEntity) oEpisodioDao.get(Integer.parseInt(mParametros.get("id_episodio"))));
		oProcedimientoEntity.setId_usuario((UsuarioEntity) oUsuarioDao.get(Integer.parseInt(mParametros.get("id_usuario"))));

		if(oProcediminetoDao.update(oProcedimientoEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oProcediminetoDao.delete(id);
	}

}

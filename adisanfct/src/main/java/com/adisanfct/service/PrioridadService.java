package com.adisanfct.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.PrioridadDao;
import com.adisanfct.entity.PrioridadEntity;

@Service
public class PrioridadService{
	@Autowired
	PrioridadDao oPrioridadDao;
	
	public 	PrioridadEntity get(int id) {
		return (PrioridadEntity) oPrioridadDao.get(id);
	}

	@SuppressWarnings("unchecked")
	public List<PrioridadEntity> getall() {
		return oPrioridadDao.getall();
	}


}

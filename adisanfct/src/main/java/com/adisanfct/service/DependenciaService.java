package com.adisanfct.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.DependenciaDao;
import com.adisanfct.entity.DependenciaEntity;

@Service
public class DependenciaService{
	@Autowired
	DependenciaDao oDependenciaDao;
	
	public 	DependenciaEntity get(int id) {
		return (DependenciaEntity) oDependenciaDao.get(id);
	}

	@SuppressWarnings("unchecked")
	public List<DependenciaEntity> getall() {
		return oDependenciaDao.getall();
	}

	public List<DependenciaEntity> getFilter(int centrosanitario) {
		return oDependenciaDao.getFilter(centrosanitario);
	}


}

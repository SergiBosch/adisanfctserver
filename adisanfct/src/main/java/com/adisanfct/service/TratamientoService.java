package com.adisanfct.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.EpisodioDao;
import com.adisanfct.dao.implementation.specific.MedicamentoDao;
import com.adisanfct.dao.implementation.specific.PosologiaDao;
import com.adisanfct.dao.implementation.specific.TratamientoDao;
import com.adisanfct.dao.implementation.specific.UsuarioDao;
import com.adisanfct.dao.implementation.specific.ViaDao;
import com.adisanfct.entity.EpisodioEntity;
import com.adisanfct.entity.MedicamentoEntity;
import com.adisanfct.entity.PosologiaEntity;
import com.adisanfct.entity.TratamientoEntity;
import com.adisanfct.entity.UsuarioEntity;
import com.adisanfct.entity.ViaEntity;

@Service
public class TratamientoService{
	@Autowired
	TratamientoDao oTratamientoDao;
	
	@Autowired
	EpisodioDao oEpisodioDao;
	
	@Autowired
	MedicamentoDao oMedicamentoDao;
	
	@Autowired
	PosologiaDao oPosologiaDao;
	
	@Autowired
	ViaDao oViaDao;
	
	@Autowired
	UsuarioDao oUsuarioDao;
	
	public TratamientoEntity get(int id) {
		return (TratamientoEntity) oTratamientoDao.get(id);
	}

	public List<TratamientoEntity> getall() {
		return oTratamientoDao.getall();
	}

	public Long count() {
		return oTratamientoDao.count();
	}

	public Page<TratamientoEntity> getPage(Pageable oPageable) {
		return oTratamientoDao.getPage(oPageable);
	}
	
	@SuppressWarnings("deprecation")
	public boolean insert(Map<String, String> mParametros) {
		TratamientoEntity oTratamientoEntity = new TratamientoEntity();
		if(mParametros.get("fecha_inicio")!="" && mParametros.get("fecha_inicio")!=null) {
		oTratamientoEntity.setFecha_inicio(new Date(mParametros.get("fecha_inicio")));
		}
		if(mParametros.get("fecha_fin")!="" && mParametros.get("fecha_fin")!=null) {
		oTratamientoEntity.setFecha_fin(new Date(mParametros.get("fecha_fin")));
		}
		oTratamientoEntity.setCuidados(mParametros.get("cuidados"));
		oTratamientoEntity.setImporte(Double.parseDouble(mParametros.get("importe")));
		oTratamientoEntity.setId_episodio((EpisodioEntity) oEpisodioDao.get(Integer.parseInt(mParametros.get("id_episodio"))));
		oTratamientoEntity.setId_medicamento((MedicamentoEntity) oMedicamentoDao.get(Integer.parseInt(mParametros.get("id_medicamento"))));
		oTratamientoEntity.setId_posologia((PosologiaEntity) oPosologiaDao.get(Integer.parseInt(mParametros.get("id_posologia"))));
		oTratamientoEntity.setId_via((ViaEntity) oViaDao.get(Integer.parseInt(mParametros.get("id_via"))));
		oTratamientoEntity.setId_usuario((UsuarioEntity) oUsuarioDao.get(Integer.parseInt(mParametros.get("id_usuario"))));

		if(oTratamientoDao.create(oTratamientoEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	@SuppressWarnings("deprecation")
	public boolean update(Map<String, String> mParametros) {
		TratamientoEntity oTratamientoEntity = new TratamientoEntity();
		oTratamientoEntity.setId(Integer.parseInt(mParametros.get("id")));
		if(mParametros.get("fecha_inicio")!="" && mParametros.get("fecha_inicio")!=null) {
			oTratamientoEntity.setFecha_inicio(new Date(mParametros.get("fecha_inicio")));
		}
		if(mParametros.get("fecha_fin")!="" && mParametros.get("fecha_fin")!=null) {
			oTratamientoEntity.setFecha_fin(new Date(mParametros.get("fecha_fin")));
		}
		oTratamientoEntity.setCuidados(mParametros.get("cuidados"));
		oTratamientoEntity.setImporte(Double.parseDouble(mParametros.get("importe")));
		oTratamientoEntity.setId_episodio((EpisodioEntity) oEpisodioDao.get(Integer.parseInt(mParametros.get("id_episodio"))));
		oTratamientoEntity.setId_medicamento((MedicamentoEntity) oMedicamentoDao.get(Integer.parseInt(mParametros.get("id_medicamento"))));
		oTratamientoEntity.setId_posologia((PosologiaEntity) oPosologiaDao.get(Integer.parseInt(mParametros.get("id_posologia"))));
		oTratamientoEntity.setId_via((ViaEntity) oViaDao.get(Integer.parseInt(mParametros.get("id_via"))));
		oTratamientoEntity.setId_usuario((UsuarioEntity) oUsuarioDao.get(Integer.parseInt(mParametros.get("id_usuario"))));

		if(oTratamientoDao.update(oTratamientoEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oTratamientoDao.delete(id);
	}

}

package com.adisanfct.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.entity.GenericEntityInterface;

public abstract class GenericService {
	
	GenericDaoImplementation<GenericEntityInterface> oGenericDao;

	public GenericEntityInterface get(int id) {
		return oGenericDao.get(id);
	}

	public List<GenericEntityInterface> getall() {
		return oGenericDao.getall();
	}

	public Long count() {
		return oGenericDao.count();
	}

	public Page<GenericEntityInterface> getPage(Pageable oPageable) {
		return oGenericDao.getPage(oPageable);
	}
	
	public boolean insert(GenericEntityInterface oFacturaTicketEntity) {
		if(oGenericDao.create(oFacturaTicketEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean update(GenericEntityInterface oFacturaTicketEntity) {
		if(oGenericDao.update(oFacturaTicketEntity) != null) {
			return true;
		} else {
			return false;
			}
	}
	
	public boolean delete(int id) {
		return oGenericDao.delete(id);
	}
}

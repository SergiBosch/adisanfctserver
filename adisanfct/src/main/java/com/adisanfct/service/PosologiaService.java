package com.adisanfct.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.adisanfct.dao.implementation.specific.PosologiaDao;
import com.adisanfct.entity.PosologiaEntity;

@Service
public class PosologiaService{
	@Autowired
	PosologiaDao oPosologiaDao;
	
	public 	PosologiaEntity get(int id) {
		return (PosologiaEntity) oPosologiaDao.get(id);
	}

	@SuppressWarnings("unchecked")
	public List<PosologiaEntity> getall() {
		return oPosologiaDao.getall();
	}


}

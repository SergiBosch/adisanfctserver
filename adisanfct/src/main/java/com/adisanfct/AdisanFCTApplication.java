package com.adisanfct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdisanFCTApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(AdisanFCTApplication.class, args);
	}

}

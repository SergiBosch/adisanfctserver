package com.adisanfct.dao.implementation.specific;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.PrioridadDaoJpaInterface;

@Repository
public class PrioridadDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected PrioridadDaoJpaInterface oJpaRepository;

	@Autowired
	public PrioridadDao(PrioridadDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

}

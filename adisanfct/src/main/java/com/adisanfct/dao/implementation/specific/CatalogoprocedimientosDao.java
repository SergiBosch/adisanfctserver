package com.adisanfct.dao.implementation.specific;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.CatalogoprocedimientosDaoJpaInterface;

@Repository
public class CatalogoprocedimientosDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected CatalogoprocedimientosDaoJpaInterface oJpaRepository;

	@Autowired
	public CatalogoprocedimientosDao(CatalogoprocedimientosDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

}

package com.adisanfct.dao.implementation.specific;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.TratamientoDaoJpaInterface;

@Repository
public class TratamientoDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected TratamientoDaoJpaInterface oJpaRepository;

	@Autowired
	public TratamientoDao(TratamientoDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

}

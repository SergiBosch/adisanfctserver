package com.adisanfct.dao.implementation.specific;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.PosologiaDaoJpaInterface;

@Repository
public class PosologiaDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected PosologiaDaoJpaInterface oJpaRepository;

	@Autowired
	public PosologiaDao(PosologiaDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

}

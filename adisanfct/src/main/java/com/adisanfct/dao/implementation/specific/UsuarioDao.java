package com.adisanfct.dao.implementation.specific;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.UsuarioDaoJpaInterface;
import com.adisanfct.entity.UsuarioEntity;

@Repository
public class UsuarioDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected UsuarioDaoJpaInterface oJpaRepository;

	@Autowired
	public UsuarioDao(UsuarioDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

	public UsuarioEntity login(String login, String password) {
		return oJpaRepository.findByLogin(login,password);
	}
}

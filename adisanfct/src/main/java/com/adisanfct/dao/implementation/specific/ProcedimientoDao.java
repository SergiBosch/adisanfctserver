package com.adisanfct.dao.implementation.specific;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.ProcedimientoDaoJpaInterface;

@Repository
public class ProcedimientoDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected ProcedimientoDaoJpaInterface oJpaRepository;

	@Autowired
	public ProcedimientoDao(ProcedimientoDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

}

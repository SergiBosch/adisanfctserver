package com.adisanfct.dao.implementation.specific;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.DependenciaDaoJpaInterface;
import com.adisanfct.entity.DependenciaEntity;

@Repository
public class DependenciaDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected DependenciaDaoJpaInterface oJpaRepository;

	@Autowired
	public DependenciaDao(DependenciaDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

	public List<DependenciaEntity> getFilter(int centrosanitario) {
		return oJpaRepository.getFilter(centrosanitario);
	}

}

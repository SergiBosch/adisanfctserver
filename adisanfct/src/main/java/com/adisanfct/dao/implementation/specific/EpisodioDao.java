package com.adisanfct.dao.implementation.specific;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.EpisodioDaoJpaInterface;
import com.adisanfct.entity.EpisodioEntity;
import com.adisanfct.entity.PacienteEntity;

@Repository
public class EpisodioDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected EpisodioDaoJpaInterface oJpaRepository;

	@Autowired
	public EpisodioDao(EpisodioDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

	public List<EpisodioEntity> getEpisodiosPaciente(PacienteEntity paciente, Pageable oPageable) {
		return oJpaRepository.getPageEpisodiosPaciente(paciente, oPageable);
	}

	public Long countEpisodioPaciente(PacienteEntity oPacienteEntity) {
		return oJpaRepository.countEpisodioPaciente(oPacienteEntity);
	}

}

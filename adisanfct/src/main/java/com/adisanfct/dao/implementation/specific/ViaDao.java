package com.adisanfct.dao.implementation.specific;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.ViaDaoJpaInterface;

@Repository
public class ViaDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected ViaDaoJpaInterface oJpaRepository;

	@Autowired
	public ViaDao(ViaDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

}

package com.adisanfct.dao.implementation.specific;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.PacienteDaoJpaInterface;
import com.adisanfct.entity.PacienteEntity;

@Repository
public class PacienteDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected PacienteDaoJpaInterface oJpaRepository;

	@Autowired
	public PacienteDao(PacienteDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

	public Page<PacienteEntity> getPageFilter(Pageable oPageable, String filtro) {
		return oJpaRepository.getFilter("%"+filtro+"%", oPageable);
	}

	public Long countFilter(String filter) {
		return oJpaRepository.countFilter("%"+filter+"%");
	}



}

package com.adisanfct.dao.implementation.specific;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.MedicamentoDaoJpaInterface;

@Repository
public class MedicamentoDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected MedicamentoDaoJpaInterface oJpaRepository;

	@Autowired
	public MedicamentoDao(MedicamentoDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

}

package com.adisanfct.dao.implementation.specific;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.AnaliticaDaoJpaInterface;

@Repository
public class AnaliticaDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected AnaliticaDaoJpaInterface oJpaRepository;

	@Autowired
	public AnaliticaDao(AnaliticaDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

}

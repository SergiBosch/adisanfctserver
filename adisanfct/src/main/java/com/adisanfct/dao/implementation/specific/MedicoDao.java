package com.adisanfct.dao.implementation.specific;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.MedicoDaoJpaInterface;
import com.adisanfct.entity.MedicoEntity;

@Repository
public class MedicoDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected MedicoDaoJpaInterface oJpaRepository;

	@Autowired
	public MedicoDao(MedicoDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

	public List<MedicoEntity> getFilter(int centrosanitario) {
		return oJpaRepository.getFilter(centrosanitario);
	}

}

package com.adisanfct.dao.implementation.specific;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.adisanfct.dao.implementation.generic.GenericDaoImplementation;
import com.adisanfct.dao.interfaces.generic.GenericDaoInterface;
import com.adisanfct.dao.interfaces.specific.CatalogoanaliticasDaoJpaInterface;

@Repository
public class CatalogoanaliticasDao extends GenericDaoImplementation implements GenericDaoInterface {

	@Autowired
	protected CatalogoanaliticasDaoJpaInterface oJpaRepository;

	@Autowired
	public CatalogoanaliticasDao(CatalogoanaliticasDaoJpaInterface oJpaRepository) {
		super(oJpaRepository);
	}

}

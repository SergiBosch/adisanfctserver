package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adisanfct.entity.CatalogoanaliticasEntity;

public interface CatalogoanaliticasDaoJpaInterface extends JpaRepository<CatalogoanaliticasEntity, Long> {	

}

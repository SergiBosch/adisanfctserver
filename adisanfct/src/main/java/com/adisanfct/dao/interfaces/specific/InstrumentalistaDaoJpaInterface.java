package com.adisanfct.dao.interfaces.specific;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.adisanfct.entity.InstrumentalistaEntity;

public interface InstrumentalistaDaoJpaInterface extends JpaRepository<InstrumentalistaEntity, Long> {	
	@Query(value="SELECT i FROM InstrumentalistaEntity i WHERE i.id_centrosanitario = :centrosanitario") 
	List<InstrumentalistaEntity> getFilter(@Param("centrosanitario") int centrosanitario);
}

package com.adisanfct.dao.interfaces.specific;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adisanfct.entity.EpisodioEntity;
import com.adisanfct.entity.PacienteEntity;

public interface EpisodioDaoJpaInterface extends JpaRepository<EpisodioEntity, Long> {
	@Query(value="SELECT * FROM Episodio e WHERE e.id_paciente = :paciente",
		    countQuery = "SELECT count(*) FROM Episodio e WHERE e.id_paciente = :paciente",
		    nativeQuery = true) 
	List<EpisodioEntity> getPageEpisodiosPaciente(@Param("paciente") PacienteEntity paciente, Pageable pageable);

	@Query(value="SELECT count(e) FROM EpisodioEntity e WHERE e.id_paciente = :paciente") 
	Long countEpisodioPaciente(@Param("paciente") PacienteEntity oPacienteEntity);	

}

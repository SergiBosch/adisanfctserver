package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adisanfct.entity.PosologiaEntity;

public interface PosologiaDaoJpaInterface extends JpaRepository<PosologiaEntity, Long> {	

}

package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adisanfct.entity.AnaliticaEntity;

public interface AnaliticaDaoJpaInterface extends JpaRepository<AnaliticaEntity, Long> {	

}

package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adisanfct.entity.ViaEntity;

public interface ViaDaoJpaInterface extends JpaRepository<ViaEntity, Long> {	

}

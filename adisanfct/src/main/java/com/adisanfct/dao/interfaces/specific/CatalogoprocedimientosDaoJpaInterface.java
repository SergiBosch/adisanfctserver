package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adisanfct.entity.CatalogoprocedimientosEntity;

public interface CatalogoprocedimientosDaoJpaInterface extends JpaRepository<CatalogoprocedimientosEntity, Long> {	

}

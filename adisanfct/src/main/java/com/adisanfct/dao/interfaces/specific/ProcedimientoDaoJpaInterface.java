package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adisanfct.entity.ProcedimientoEntity;

public interface ProcedimientoDaoJpaInterface extends JpaRepository<ProcedimientoEntity, Long> {	

}

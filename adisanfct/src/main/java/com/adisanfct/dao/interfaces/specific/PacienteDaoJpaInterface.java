package com.adisanfct.dao.interfaces.specific;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.adisanfct.entity.PacienteEntity;

public interface PacienteDaoJpaInterface extends JpaRepository<PacienteEntity, Long> {	
	@Query(value="SELECT * FROM Paciente p WHERE p.id LIKE :filtro OR nombre LIKE :filtro OR dni LIKE :filtro",
		    countQuery = "SELECT count(*) FROM Paciente p WHERE p.id LIKE :filtro OR nombre LIKE :filtro OR dni LIKE :filtro",
		    nativeQuery = true) 
	Page<PacienteEntity> getFilter(@Param("filtro") String filtro, Pageable pageable);

	@Query(value="SELECT count(*) FROM Paciente p WHERE p.id LIKE :filtro OR nombre LIKE :filtro OR dni LIKE :filtro",
		    nativeQuery = true) 
	Long countFilter(@Param("filtro") String filtro);
}

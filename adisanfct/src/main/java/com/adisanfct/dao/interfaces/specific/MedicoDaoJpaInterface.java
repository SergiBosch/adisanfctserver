package com.adisanfct.dao.interfaces.specific;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adisanfct.entity.InstrumentalistaEntity;
import com.adisanfct.entity.MedicoEntity;

public interface MedicoDaoJpaInterface extends JpaRepository<MedicoEntity, Long> {	
	@Query(value="SELECT m FROM MedicoEntity m WHERE m.id_centrosanitario = :centrosanitario") 
	List<MedicoEntity> getFilter(@Param("centrosanitario") int centrosanitario);
}

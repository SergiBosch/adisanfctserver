package com.adisanfct.dao.interfaces.specific;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.adisanfct.entity.DependenciaEntity;

public interface DependenciaDaoJpaInterface extends JpaRepository<DependenciaEntity, Long> {	
	@Query(value="SELECT d FROM DependenciaEntity d WHERE d.id_centrosanitario = :centrosanitario") 
	List<DependenciaEntity> getFilter(@Param("centrosanitario") int centrosanitario);
}

package com.adisanfct.dao.interfaces.generic;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.adisanfct.entity.GenericEntityInterface;

public interface GenericDaoInterface <T extends GenericEntityInterface> {

	T get(int id);

	List<T> getall();

	long count();

	Page<T> getPage(Pageable oPageable);

	Boolean delete(int id);

	T create(T oEntity);

	T update(T oEntity);

}

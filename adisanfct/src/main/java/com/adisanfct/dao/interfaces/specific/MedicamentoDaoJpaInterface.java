package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adisanfct.entity.MedicamentoEntity;

public interface MedicamentoDaoJpaInterface extends JpaRepository<MedicamentoEntity, Long> {	

}

package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adisanfct.entity.TratamientoEntity;

public interface TratamientoDaoJpaInterface extends JpaRepository<TratamientoEntity, Long> {	
}

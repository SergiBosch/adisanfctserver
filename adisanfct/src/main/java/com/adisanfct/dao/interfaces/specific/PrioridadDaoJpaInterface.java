package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import com.adisanfct.entity.PrioridadEntity;

public interface PrioridadDaoJpaInterface extends JpaRepository<PrioridadEntity, Long> {	

}

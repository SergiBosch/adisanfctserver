package com.adisanfct.dao.interfaces.specific;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.adisanfct.entity.UsuarioEntity;

public interface UsuarioDaoJpaInterface extends JpaRepository<UsuarioEntity, Long> {	
	@Query(value="SELECT u FROM UsuarioEntity u WHERE u.login = :login AND u.password = :password") 
	public UsuarioEntity findByLogin(@Param("login") String login, @Param("password") String password);
}

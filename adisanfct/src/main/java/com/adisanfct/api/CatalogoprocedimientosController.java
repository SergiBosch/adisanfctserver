
package com.adisanfct.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.CatalogoprocedimientosEntity;
import com.adisanfct.service.CatalogoprocedimientosService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/catalogoprocedimientos")
public class CatalogoprocedimientosController {

	@Autowired
	CatalogoprocedimientosService oCatalogoprocedimientosService;

	@GetMapping("/get/{id}")
	public ResponseEntity<CatalogoprocedimientosEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<CatalogoprocedimientosEntity>((CatalogoprocedimientosEntity) oCatalogoprocedimientosService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<CatalogoprocedimientosEntity>> getAll() {
		return new ResponseEntity<List<CatalogoprocedimientosEntity>>(oCatalogoprocedimientosService.getall(), HttpStatus.OK);
	}
}

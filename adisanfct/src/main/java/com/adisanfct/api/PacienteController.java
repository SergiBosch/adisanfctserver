
package com.adisanfct.api;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.PacienteEntity;
import com.adisanfct.service.PacienteService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/paciente")
public class PacienteController {

	@Autowired
	PacienteService oPacienteService;

	@GetMapping("/get/{id}")
	public ResponseEntity<PacienteEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<PacienteEntity>((PacienteEntity) oPacienteService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<PacienteEntity>> getAll() {
		return new ResponseEntity<List<PacienteEntity>>(oPacienteService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oPacienteService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/count/{filter}")
	public ResponseEntity<Long> count(@PathVariable(value = "filter") String filter) {
		return new ResponseEntity<Long>(oPacienteService.countFilter(filter), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<List<PacienteEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<PacienteEntity>>(oPacienteService.getPage(oPageable).getContent(), HttpStatus.OK);
	}
	
	@GetMapping("/getpage/{page}/{rpp}/{filtro}")
	public ResponseEntity<List<PacienteEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp, @PathVariable(value="filtro") String filtro) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<PacienteEntity>>(oPacienteService.getPageFilter(oPageable, filtro).getContent(), HttpStatus.OK);
	}
	
	@GetMapping("/getpage/{page}/{rpp}/{col}/{order}")
	public ResponseEntity<List<PacienteEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp, @PathVariable(value = "col") String col, @PathVariable(value = "order") String order) {
		Pageable oPageable;
		String columna = col;
		if (col.equalsIgnoreCase("apellidos")) {
		columna = "primerApellido";
		}
		if (order.equalsIgnoreCase("asc")) {
			oPageable = PageRequest.of(page, rpp, Sort.by(columna));
		}else {
			oPageable = PageRequest.of(page, rpp, Sort.by(columna).descending());	
		}
		return new ResponseEntity<List<PacienteEntity>>(oPacienteService.getPage(oPageable).getContent(), HttpStatus.OK);

	}
	
	@GetMapping("/getpage/{page}/{rpp}/{filtro}/{col}/{order}")
	public ResponseEntity<List<PacienteEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp, @PathVariable(value="filtro") String filtro,
			@PathVariable(value = "col") String col, @PathVariable(value = "order") String order) {
		Pageable oPageable;
		String columna = col;
		if (col.equalsIgnoreCase("apellidos")) {
		columna = "primerApellido";
		}
		if (order.equalsIgnoreCase("asc")) {
			oPageable = PageRequest.of(page, rpp, Sort.by(columna));
		}else {
			oPageable = PageRequest.of(page, rpp, Sort.by(columna).descending());	
		}
		return new ResponseEntity<List<PacienteEntity>>(oPacienteService.getPageFilter(oPageable, filtro).getContent(), HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Boolean> insert(@RequestBody Map<String, String> mParametros) {
		return new ResponseEntity<Boolean>(oPacienteService.insert(mParametros), HttpStatus.OK);
	}
	
	@PutMapping("/")
	public ResponseEntity<Boolean> update(@RequestBody Map<String, String> mParametros) {
		return new ResponseEntity<Boolean>(oPacienteService.update(mParametros), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") int id) {
		return new ResponseEntity<Boolean>(oPacienteService.delete(id), HttpStatus.OK);
	}

}

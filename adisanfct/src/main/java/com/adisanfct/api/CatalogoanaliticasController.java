
package com.adisanfct.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.CatalogoanaliticasEntity;
import com.adisanfct.service.CatalogoanaliticasService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/catalogoanaliticas")
public class CatalogoanaliticasController {

	@Autowired
	CatalogoanaliticasService oCatalogoanaliticasService;

	@GetMapping("/get/{id}")
	public ResponseEntity<CatalogoanaliticasEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<CatalogoanaliticasEntity>((CatalogoanaliticasEntity) oCatalogoanaliticasService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<CatalogoanaliticasEntity>> getAll() {
		return new ResponseEntity<List<CatalogoanaliticasEntity>>(oCatalogoanaliticasService.getall(), HttpStatus.OK);
	}
}

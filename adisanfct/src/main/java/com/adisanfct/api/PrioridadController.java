
package com.adisanfct.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.PrioridadEntity;
import com.adisanfct.service.PrioridadService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/prioridad")
public class PrioridadController {

	@Autowired
	PrioridadService oPrioridadService;

	@GetMapping("/get/{id}")
	public ResponseEntity<PrioridadEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<PrioridadEntity>((PrioridadEntity) oPrioridadService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<PrioridadEntity>> getAll() {
		return new ResponseEntity<List<PrioridadEntity>>(oPrioridadService.getall(), HttpStatus.OK);
	}
}


package com.adisanfct.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adisanfct.entity.InstrumentalistaEntity;
import com.adisanfct.entity.MedicoEntity;
import com.adisanfct.service.MedicoService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/medico")
public class MedicoController {

	@Autowired
	MedicoService oMedicoService;

	@GetMapping("/get/{id}")
	public ResponseEntity<MedicoEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<MedicoEntity>((MedicoEntity) oMedicoService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<MedicoEntity>> getAll() {
		return new ResponseEntity<List<MedicoEntity>>(oMedicoService.getall(), HttpStatus.OK);
	}
	
	@GetMapping("/getfilter/{centrosanitario}")
	public ResponseEntity<List<MedicoEntity>> getFilter(@PathVariable(value = "centrosanitario") int centrosanitario) {
		return new ResponseEntity<List<MedicoEntity>>(oMedicoService.getFilter(centrosanitario), HttpStatus.OK);
	}
}

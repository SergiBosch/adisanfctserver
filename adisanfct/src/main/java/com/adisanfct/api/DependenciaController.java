
package com.adisanfct.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.DependenciaEntity;
import com.adisanfct.entity.InstrumentalistaEntity;
import com.adisanfct.service.DependenciaService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/dependencia")
public class DependenciaController {

	@Autowired
	DependenciaService oDependenciaService;

	@GetMapping("/get/{id}")
	public ResponseEntity<DependenciaEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<DependenciaEntity>((DependenciaEntity) oDependenciaService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<DependenciaEntity>> getAll() {
		return new ResponseEntity<List<DependenciaEntity>>(oDependenciaService.getall(), HttpStatus.OK);
	}
	
	@GetMapping("/getfilter/{centrosanitario}")
	public ResponseEntity<List<DependenciaEntity>> getFilter(@PathVariable(value = "centrosanitario") int centrosanitario) {
		return new ResponseEntity<List<DependenciaEntity>>(oDependenciaService.getFilter(centrosanitario), HttpStatus.OK);
	}
}

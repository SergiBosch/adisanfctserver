
package com.adisanfct.api;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.TratamientoEntity;
import com.adisanfct.service.TratamientoService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/tratamiento")
public class TratamientoController {

	@Autowired
	TratamientoService oTratamientoService;

	@GetMapping("/get/{id}")
	public ResponseEntity<TratamientoEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<TratamientoEntity>((TratamientoEntity) oTratamientoService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<TratamientoEntity>> getAll() {
		return new ResponseEntity<List<TratamientoEntity>>(oTratamientoService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oTratamientoService.count(), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<List<TratamientoEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<TratamientoEntity>>(oTratamientoService.getPage(oPageable).getContent(), HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Boolean> insert(@RequestBody Map<String, String> mParametros) {
		return new ResponseEntity<Boolean>(oTratamientoService.insert(mParametros), HttpStatus.OK);
	}
	
	@PutMapping("/")
	public ResponseEntity<Boolean> update(@RequestBody Map<String, String> mParametros) {
		return new ResponseEntity<Boolean>(oTratamientoService.update(mParametros), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") int id) {
		return new ResponseEntity<Boolean>(oTratamientoService.delete(id), HttpStatus.OK);
	}

}

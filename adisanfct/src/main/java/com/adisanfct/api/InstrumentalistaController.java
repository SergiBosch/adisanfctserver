
package com.adisanfct.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.InstrumentalistaEntity;
import com.adisanfct.service.InstrumentalistaService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/instrumentalista")
public class InstrumentalistaController {

	@Autowired
	InstrumentalistaService oInstrumentalistaService;

	@GetMapping("/get/{id}")
	public ResponseEntity<InstrumentalistaEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<InstrumentalistaEntity>((InstrumentalistaEntity) oInstrumentalistaService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<InstrumentalistaEntity>> getAll() {
		return new ResponseEntity<List<InstrumentalistaEntity>>(oInstrumentalistaService.getall(), HttpStatus.OK);
	}
	
	@GetMapping("/getfilter/{centrosanitario}")
	public ResponseEntity<List<InstrumentalistaEntity>> getFilter(@PathVariable(value = "centrosanitario") int centrosanitario) {
		return new ResponseEntity<List<InstrumentalistaEntity>>(oInstrumentalistaService.getFilter(centrosanitario), HttpStatus.OK);
	}
}

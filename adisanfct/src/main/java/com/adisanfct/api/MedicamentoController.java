
package com.adisanfct.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.MedicamentoEntity;
import com.adisanfct.service.MedicamentoService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/medicamento")
public class MedicamentoController {

	@Autowired
	MedicamentoService oMedicamentoService;

	@GetMapping("/get/{id}")
	public ResponseEntity<MedicamentoEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<MedicamentoEntity>((MedicamentoEntity) oMedicamentoService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<MedicamentoEntity>> getAll() {
		return new ResponseEntity<List<MedicamentoEntity>>(oMedicamentoService.getall(), HttpStatus.OK);
	}
}


package com.adisanfct.api;

import java.util.Map;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.UsuarioEntity;
import com.adisanfct.service.UsuarioService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/session")
public class SessionController {

	@Autowired
	HttpSession oSession;

	@Autowired
	UsuarioService oUsuarioService;

	@GetMapping("/") // check
	public ResponseEntity<UsuarioEntity> check() {
		UsuarioEntity oUsuarioEntity = (UsuarioEntity) oSession.getAttribute("login");
		return new ResponseEntity<UsuarioEntity>(oUsuarioEntity, HttpStatus.OK);
	}

	@DeleteMapping("/") // logout
	public ResponseEntity<Boolean> logout() {
		oSession.invalidate();
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

	@PostMapping("/") // login
	public ResponseEntity<UsuarioEntity> login(@RequestBody Map<String, String> mParametros) {
		oSession.setAttribute("login", oUsuarioService.login(mParametros));
		return new ResponseEntity<UsuarioEntity>((UsuarioEntity) oSession.getAttribute("login"), HttpStatus.OK);
	}
}


package com.adisanfct.api;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.PosologiaEntity;
import com.adisanfct.service.PosologiaService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/posologia")
public class PosologiaController {

	@Autowired
	PosologiaService oPosologiaService;

	@GetMapping("/get/{id}")
	public ResponseEntity<PosologiaEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<PosologiaEntity>((PosologiaEntity) oPosologiaService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<PosologiaEntity>> getAll() {
		return new ResponseEntity<List<PosologiaEntity>>(oPosologiaService.getall(), HttpStatus.OK);
	}
}

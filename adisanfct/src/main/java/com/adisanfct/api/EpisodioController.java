
package com.adisanfct.api;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.adisanfct.entity.EpisodioEntity;
import com.adisanfct.service.EpisodioService;

@CrossOrigin(origins="*",maxAge=3600, allowCredentials = "true")
@RestController
@RequestMapping("/episodio")
public class EpisodioController {

	@Autowired
	EpisodioService oEpisodioService;

	@GetMapping("/get/{id}")
	public ResponseEntity<EpisodioEntity> get(@PathVariable(value = "id") int id) {
		return new ResponseEntity<EpisodioEntity>((EpisodioEntity) oEpisodioService.get(id), HttpStatus.OK);
	}

	@GetMapping("/getall")
	public ResponseEntity<List<EpisodioEntity>> getAll() {
		return new ResponseEntity<List<EpisodioEntity>>(oEpisodioService.getall(), HttpStatus.OK);
	}

	@GetMapping("/count")
	public ResponseEntity<Long> count() {
		return new ResponseEntity<Long>(oEpisodioService.count(), HttpStatus.OK);
	}
	
	@GetMapping("/countepisodiopaciente/{paciente}")
	public ResponseEntity<Long> countEpisodioPaciente(@PathVariable(value = "paciente") int paciente) {
		return new ResponseEntity<Long>(oEpisodioService.countEpisodioPaciente(paciente), HttpStatus.OK);
	}

	@GetMapping("/getpage/{page}/{rpp}")
	public ResponseEntity<List<EpisodioEntity>> getPage(@PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<EpisodioEntity>>(oEpisodioService.getPage(oPageable).getContent(), HttpStatus.OK);
	}
	
	@GetMapping("/episodiospaciente/{paciente}/{page}/{rpp}")
	public ResponseEntity<List<EpisodioEntity>> getPage(@PathVariable(value = "paciente") int paciente, @PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp) {
		Pageable oPageable;
		oPageable = PageRequest.of(page, rpp);
		return new ResponseEntity<List<EpisodioEntity>>(oEpisodioService.getEpisodiosPaciente(paciente, oPageable), HttpStatus.OK);
	}
	
	@GetMapping("/episodiospaciente/{paciente}/{page}/{rpp}/{col}/{order}")
	public ResponseEntity<List<EpisodioEntity>> getPage(@PathVariable(value = "paciente") int paciente, @PathVariable(value = "page") int page,
			@PathVariable(value = "rpp") int rpp, @PathVariable(value = "col") String col, @PathVariable(value = "order") String order) {
		Pageable oPageable;
		String columna = col;
		if (col.equalsIgnoreCase("firma")) {
			columna = "fecha_firma";
		} else if (col.equalsIgnoreCase("inicio")) {
			columna = "fecha_inicio";
		} else if (col.equalsIgnoreCase("alta")) {
			columna = "fecha_alta";
		}
		if (order.equalsIgnoreCase("asc")) {
			oPageable = PageRequest.of(page, rpp, Sort.by(columna));
		}else {
			oPageable = PageRequest.of(page, rpp, Sort.by(columna).descending());	
		}
		return new ResponseEntity<List<EpisodioEntity>>(oEpisodioService.getEpisodiosPaciente(paciente, oPageable), HttpStatus.OK);
	}
	
	@PostMapping("/")
	public ResponseEntity<Boolean> insert(@RequestBody Map<String, String> mParametros) {
		return new ResponseEntity<Boolean>(oEpisodioService.insert(mParametros), HttpStatus.OK);
	}
	
	@PutMapping("/")
	public ResponseEntity<Boolean> update(@RequestBody Map<String, String> mParametros) {
		return new ResponseEntity<Boolean>(oEpisodioService.update(mParametros), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> delete(@PathVariable(value = "id") int id) {
		return new ResponseEntity<Boolean>(oEpisodioService.delete(id), HttpStatus.OK);
	}

}

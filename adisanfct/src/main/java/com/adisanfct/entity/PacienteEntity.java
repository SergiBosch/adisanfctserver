package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "paciente")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PacienteEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "dni")
	private String dni;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "primer_apellido")
	private String primerApellido;
	@Column(name = "segundo_apellido")
	private String segundo_apellido;
	@Column(name = "direccion")
	private String direccion;
	@Column(name = "ciudad")
	private String ciudad;
	@Column(name = "codigo_postal")
	private String codigo_postal;
	@Column(name = "provincia")
	private String provincia;
	@Column(name = "pais")
	private String pais;
	@Column(name = "email")
	private String email;
	@Column(name = "telefono1")
	private String telefono1;
	@Column(name = "telefono2")
	private String telefono2;
	@Column(name = "nombre_padre")
	private String nombre_padre;
	@Column(name = "nombre_madre")
	private String nombre_madre;
	@Column(name = "fecha_nacimiento")
	private Date fecha_nacimiento;
	@Column(name = "ciudad_nacimiento")
	private String ciudad_nacimiento;
	@Column(name = "pais_nacimiento")
	private String pais_nacimiento;
	@Column(name = "sip_aseguradora")
	private String sip_aseguradora;
	@Column(name = "id_tipopago")
	private Integer id_tipopago;
	@Column(name = "id_sexo")
	private Integer id_sexo;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "id_usuario")
	private UsuarioEntity id_usuario;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_paciente", cascade = { CascadeType.ALL })
	private List<EpisodioEntity> lista_episodio = new ArrayList<>();

	public PacienteEntity() {
		super();
	}

	public PacienteEntity(int id, String dni, String nombre, String primerApellido, String segundo_apellido,
			String direccion, String ciudad, String codigo_postal, String provincia, String pais, String email,
			String telefono1, String telefono2, String nombre_padre, String nombre_madre, Date fecha_nacimiento,
			String ciudad_nacimiento, String pais_nacimiento, String sip_aseguradora, Integer id_tipopago,
			Integer id_sexo, UsuarioEntity id_usuario) {
		super();
		this.id = id;
		this.dni = dni;
		this.nombre = nombre;
		this.primerApellido = primerApellido;
		this.segundo_apellido = segundo_apellido;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.codigo_postal = codigo_postal;
		this.provincia = provincia;
		this.pais = pais;
		this.email = email;
		this.telefono1 = telefono1;
		this.telefono2 = telefono2;
		this.nombre_padre = nombre_padre;
		this.nombre_madre = nombre_madre;
		this.fecha_nacimiento = fecha_nacimiento;
		this.ciudad_nacimiento = ciudad_nacimiento;
		this.pais_nacimiento = pais_nacimiento;
		this.sip_aseguradora = sip_aseguradora;
		this.id_tipopago = id_tipopago;
		this.id_sexo = id_sexo;
		this.id_usuario = id_usuario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimer_apellido() {
		return primerApellido;
	}

	public void setPrimer_apellido(String primer_apellido) {
		this.primerApellido = primer_apellido;
	}

	public String getSegundo_apellido() {
		return segundo_apellido;
	}

	public void setSegundo_apellido(String segundo_apellido) {
		this.segundo_apellido = segundo_apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(String codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono1() {
		return telefono1;
	}

	public void setTelefono1(String telefono1) {
		this.telefono1 = telefono1;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getNombre_padre() {
		return nombre_padre;
	}

	public void setNombre_padre(String nombre_padre) {
		this.nombre_padre = nombre_padre;
	}

	public String getNombre_madre() {
		return nombre_madre;
	}

	public void setNombre_madre(String nombre_madre) {
		this.nombre_madre = nombre_madre;
	}

	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getCiudad_nacimiento() {
		return ciudad_nacimiento;
	}

	public void setCiudad_nacimiento(String ciudad_nacimiento) {
		this.ciudad_nacimiento = ciudad_nacimiento;
	}

	public String getPais_nacimiento() {
		return pais_nacimiento;
	}

	public void setPais_nacimiento(String pais_nacimiento) {
		this.pais_nacimiento = pais_nacimiento;
	}

	public String getSip_aseguradora() {
		return sip_aseguradora;
	}

	public void setSip_aseguradora(String sip_aseguradora) {
		this.sip_aseguradora = sip_aseguradora;
	}

	public Integer getId_tipopago() {
		return id_tipopago;
	}

	public void setId_tipopago(Integer id_tipopago) {
		this.id_tipopago = id_tipopago;
	}

	public Integer getId_sexo() {
		return id_sexo;
	}

	public void setId_sexo(Integer id_sexo) {
		this.id_sexo = id_sexo;
	}

	public Integer getId_usuario() {
		return id_usuario.getId();
	}

	public void setId_usuario(UsuarioEntity id_usuario) {
		this.id_usuario = id_usuario;
	}

	/*public Integer getLista_episodio() {
		return lista_episodio.size();
	}*/

	public void setLista_episodio(List<EpisodioEntity> lista_episodio) {
		this.lista_episodio = lista_episodio;
	}
	
	

}

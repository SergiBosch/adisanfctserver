package com.adisanfct.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "procedimiento")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ProcedimientoEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "fecha_inicio")
	private Date fecha_inicio;
	@Column(name = "fecha_fin")
	private Date fecha_fin;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_instrumentalista")
	private InstrumentalistaEntity id_instrumentalista;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_episodio")
	private EpisodioEntity id_episodio;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_medico")
	private MedicoEntity id_medico;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_dependencia")
	private DependenciaEntity id_dependencia;
	@Column(name = "informe", columnDefinition = "LONGTEXT")
	private String informe;
	@Column(name = "diagnostico_inicial", columnDefinition = "LONGTEXT")
	private String diagnostico_inicial;
	@Column(name = "diagnostico_final", columnDefinition = "LONGTEXT")
	private String diagnostico_final;
	@Column(name = "procedimiento_previsto", columnDefinition = "LONGTEXT")
	private String procedimiento_previsto;
	@Column(name = "procedimiento_realizado", columnDefinition = "LONGTEXT")
	private String procedimiento_realizado;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_prioridad")
	private PrioridadEntity id_prioridad;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_procedimiento")
	private CatalogoprocedimientosEntity id_procedimiento;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_usuario")
	private UsuarioEntity id_usuario;
	
	public ProcedimientoEntity() {
		super();
	}

	public ProcedimientoEntity(int id, Date fecha_inicio, Date fecha_fin, InstrumentalistaEntity id_instrumentalista,
			EpisodioEntity id_episodio, MedicoEntity id_medico, DependenciaEntity id_dependencia, String informe,
			String diagnostico_inicial, String diagnostico_final, String procedimiento_previsto,
			String procedimiento_realizado, PrioridadEntity id_prioridad, CatalogoprocedimientosEntity id_procedimiento) {
		super();
		this.id = id;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.id_instrumentalista = id_instrumentalista;
		this.id_episodio = id_episodio;
		this.id_medico = id_medico;
		this.id_dependencia = id_dependencia;
		this.informe = informe;
		this.diagnostico_inicial = diagnostico_inicial;
		this.diagnostico_final = diagnostico_final;
		this.procedimiento_previsto = procedimiento_previsto;
		this.procedimiento_realizado = procedimiento_realizado;
		this.id_prioridad = id_prioridad;
		this.id_procedimiento = id_procedimiento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public Date getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public InstrumentalistaEntity getId_instrumentalista() {
		return id_instrumentalista;
	}

	public void setId_instrumentalista(InstrumentalistaEntity id_instrumentalista) {
		this.id_instrumentalista = id_instrumentalista;
	}

	public Integer getId_episodio() {
		return id_episodio.getId();
	}

	public void setId_episodio(EpisodioEntity id_episodio) {
		this.id_episodio = id_episodio;
	}

	public MedicoEntity getId_medico() {
		return id_medico;
	}

	public void setId_medico(MedicoEntity id_medico) {
		this.id_medico = id_medico;
	}

	public DependenciaEntity getId_dependencia() {
		return id_dependencia;
	}

	public void setId_dependencia(DependenciaEntity id_dependencia) {
		this.id_dependencia = id_dependencia;
	}

	public String getInforme() {
		return informe;
	}

	public void setInforme(String informe) {
		this.informe = informe;
	}

	public String getDiagnostico_inicial() {
		return diagnostico_inicial;
	}

	public void setDiagnostico_inicial(String diagnostico_inicial) {
		this.diagnostico_inicial = diagnostico_inicial;
	}

	public String getDiagnostico_final() {
		return diagnostico_final;
	}

	public void setDiagnostico_final(String diagnostico_final) {
		this.diagnostico_final = diagnostico_final;
	}

	public String getProcedimiento_previsto() {
		return procedimiento_previsto;
	}

	public void setProcedimiento_previsto(String procedimiento_previsto) {
		this.procedimiento_previsto = procedimiento_previsto;
	}

	public String getProcedimiento_realizado() {
		return procedimiento_realizado;
	}

	public void setProcedimiento_realizado(String procedimiento_realizado) {
		this.procedimiento_realizado = procedimiento_realizado;
	}

	public PrioridadEntity getId_prioridad() {
		return id_prioridad;
	}

	public void setId_prioridad(PrioridadEntity id_prioridad) {
		this.id_prioridad = id_prioridad;
	}

	public CatalogoprocedimientosEntity getId_procedimiento() {
		return id_procedimiento;
	}

	public void setId_procedimiento(CatalogoprocedimientosEntity id_procedimiento) {
		this.id_procedimiento = id_procedimiento;
	}

	public void setId_usuario(UsuarioEntity id_usuario) {
		this.id_usuario = id_usuario;
	}

	
	
	
}

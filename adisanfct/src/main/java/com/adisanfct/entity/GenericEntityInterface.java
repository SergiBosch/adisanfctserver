package com.adisanfct.entity;

public interface GenericEntityInterface {

	int getId();

	void setId(int id);

}
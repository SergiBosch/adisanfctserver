package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "catalogoanaliticas")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CatalogoanaliticasEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "instrucciones")
	private String instrucciones;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_catalogoanaliticas", cascade = { CascadeType.ALL })
	private List<AnaliticaEntity> lista_analitica = new ArrayList<>();
	
	public CatalogoanaliticasEntity() {
		super();
	}

	public CatalogoanaliticasEntity(int id, String descripcion, String instrucciones) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.instrucciones = instrucciones;
	}

	public CatalogoanaliticasEntity(int id, String descripcion, String instrucciones, List<AnaliticaEntity> lista_analitica) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.instrucciones = instrucciones;
		this.lista_analitica = lista_analitica;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getInstrucciones() {
		return instrucciones;
	}

	public void setInstrucciones(String instrucciones) {
		this.instrucciones = instrucciones;
	}

	public Integer getLista_analitica() {
		return lista_analitica.size();
	}

	public void setLista_analitica(List<AnaliticaEntity> lista_analitica) {
		this.lista_analitica = lista_analitica;
	}

	

}

package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "usuario")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UsuarioEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "nombre", nullable=false)
	private String nombre;
	@Column(name = "primer_apellido", nullable=false)
	private String primer_apellido;
	@Column(name = "segundo_apellido", nullable=false)
	private String segundo_apellido;
	@Column(name = "login", nullable=false)
	private String login;
	@Column(name = "password", nullable=false)
	@JsonIgnore
	private String password;
	@Column(name = "email", nullable=false)
	private String email;
	@Column(name = "token", nullable=false)
	private String token;
	@Column(name = "activo", nullable=false)
	private Boolean activo;
	@Column(name = "fecha_alta", nullable=false)
	private Date fecha_alta;
	@Column(name = "validado", nullable=false)
	private Boolean validado;
	@Column(name = "id_tipousuario", nullable=false)
	private Integer id_tipousuario;
	@Column(name = "id_grupo", nullable=false)
	private Integer id_grupo;
	@Column(name = "id_centro", nullable=false)
	private Integer id_centro;
	@Column(name = "id_centrosanitario", nullable=false)
	private Integer id_centrosanitario;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_usuario", cascade = { CascadeType.ALL })
	private List<PacienteEntity> lista_pacientes = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_usuario", cascade = { CascadeType.ALL })
	private List<EpisodioEntity> lista_episodio = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_usuario", cascade = { CascadeType.ALL })
	private List<AnaliticaEntity> lista_analitica = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_usuario", cascade = { CascadeType.ALL })
	private List<TratamientoEntity> lista_tratamiento = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_usuario", cascade = { CascadeType.ALL })
	private List<ProcedimientoEntity> lista_procedimiento = new ArrayList<>();
	
	
	
	public UsuarioEntity() {
		super();
	}

	public UsuarioEntity(int id, String nombre, String primer_apellido, String segundo_apellido, String login,
			String password, String email, String token, Boolean activo, Date fecha_alta, Boolean validado,
			Integer id_tipousuario, Integer id_grupo, Integer id_centro, Integer id_centrosanitario) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.primer_apellido = primer_apellido;
		this.segundo_apellido = segundo_apellido;
		this.login = login;
		this.password = password;
		this.email = email;
		this.token = token;
		this.activo = activo;
		this.fecha_alta = fecha_alta;
		this.validado = validado;
		this.id_tipousuario = id_tipousuario;
		this.id_grupo = id_grupo;
		this.id_centro = id_centro;
		this.id_centrosanitario = id_centrosanitario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimer_apellido() {
		return primer_apellido;
	}

	public void setPrimer_apellido(String primer_apellido) {
		this.primer_apellido = primer_apellido;
	}

	public String getSegundo_apellido() {
		return segundo_apellido;
	}

	public void setSegundo_apellido(String segundo_apellido) {
		this.segundo_apellido = segundo_apellido;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Date getFecha_alta() {
		return fecha_alta;
	}

	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}

	public Boolean getValidado() {
		return validado;
	}

	public void setValidado(Boolean validado) {
		this.validado = validado;
	}

	public Integer getId_tipousuario() {
		return id_tipousuario;
	}

	public void setId_tipousuario(Integer id_tipousuario) {
		this.id_tipousuario = id_tipousuario;
	}

	public Integer getId_grupo() {
		return id_grupo;
	}

	public void setId_grupo(Integer id_grupo) {
		this.id_grupo = id_grupo;
	}

	public Integer getId_centro() {
		return id_centro;
	}

	public void setId_centro(Integer id_centro) {
		this.id_centro = id_centro;
	}

	public Integer getId_centrosanitario() {
		return id_centrosanitario;
	}

	public void setId_centrosanitario(Integer id_centrosanitario) {
		this.id_centrosanitario = id_centrosanitario;
	}

	public Integer getLista_pacientes() {
		return lista_pacientes.size();
	}

	public void setLista_pacientes(List<PacienteEntity> lista_pacientes) {
		this.lista_pacientes = lista_pacientes;
	}

	public Integer getLista_episodio() {
		return lista_episodio.size();
	}

	public void setLista_episodio(List<EpisodioEntity> lista_episodio) {
		this.lista_episodio = lista_episodio;
	}

	public Integer getLista_analitica() {
		return lista_analitica.size();
	}

	public void setLista_analitica(List<AnaliticaEntity> lista_analitica) {
		this.lista_analitica = lista_analitica;
	}

	public Integer getLista_tratamiento() {
		return lista_tratamiento.size();
	}

	public void setLista_tratamiento(List<TratamientoEntity> lista_tratamiento) {
		this.lista_tratamiento = lista_tratamiento;
	}

	public Integer getLista_procedimiento() {
		return lista_procedimiento.size();
	}

	public void setLista_procedimiento(List<ProcedimientoEntity> lista_procedimiento) {
		this.lista_procedimiento = lista_procedimiento;
	}
	
	
	
}

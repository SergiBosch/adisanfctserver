package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "prioridad")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PrioridadEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "descripcion")
	private String descripcion;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_prioridad", cascade = { CascadeType.ALL })
	private List<ProcedimientoEntity> lista_procedimiento = new ArrayList<>();
	
	public PrioridadEntity() {
		super();
	}

	public PrioridadEntity(int id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getLista_procedimiento() {
		return lista_procedimiento.size();
	}

	public void setLista_procedimiento(List<ProcedimientoEntity> lista_procedimiento) {
		this.lista_procedimiento = lista_procedimiento;
	}


}

package com.adisanfct.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "tratamiento")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TratamientoEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "fecha_inicio")
	private Date fecha_inicio;
	@Column(name = "fecha_fin")
	private Date fecha_fin;
	@Column(name = "cuidados", columnDefinition = "LONGTEXT")
	private String cuidados;
	@Column(name = "importe")
	private Double importe;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_medicamento")
	private MedicamentoEntity id_medicamento;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_posologia")
	private PosologiaEntity id_posologia;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_via")
	private ViaEntity id_via;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_episodio")
	private EpisodioEntity id_episodio;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_usuario")
	private UsuarioEntity id_usuario;
	
	public TratamientoEntity() {
		super();
	}

	public TratamientoEntity(int id, Date fecha_inicio, Date fecha_fin, String cuidados, Double importe,
			MedicamentoEntity id_medicamento, PosologiaEntity id_posologia, ViaEntity id_via, EpisodioEntity id_episodio) {
		super();
		this.id = id;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.cuidados = cuidados;
		this.importe = importe;
		this.id_medicamento = id_medicamento;
		this.id_posologia = id_posologia;
		this.id_via = id_via;
		this.id_episodio = id_episodio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public Date getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(Date fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public String getCuidados() {
		return cuidados;
	}

	public void setCuidados(String cuidados) {
		this.cuidados = cuidados;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public MedicamentoEntity getId_medicamento() {
		return id_medicamento;
	}

	public void setId_medicamento(MedicamentoEntity id_medicamento) {
		this.id_medicamento = id_medicamento;
	}

	public PosologiaEntity getId_posologia() {
		return id_posologia;
	}

	public void setId_posologia(PosologiaEntity id_posologia) {
		this.id_posologia = id_posologia;
	}

	public ViaEntity getId_via() {
		return id_via;
	}

	public void setId_via(ViaEntity id_via) {
		this.id_via = id_via;
	}

	public Integer getId_episodio() {
		return id_episodio.getId();
	}

	public void setId_episodio(EpisodioEntity id_episodio) {
		this.id_episodio = id_episodio;
	}

	
	public void setId_usuario(UsuarioEntity id_usuario) {
		this.id_usuario = id_usuario;
	}
	
	
	
}

package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "medico")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MedicoEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "id_servicio")
	private Integer id_servicio;
	@Column(name = "id_especialidad")
	private Integer id_especialidad;
	@Column(name = "dni")
	private String dni;
	@Column(name = "nombre")
	private String nombre;
	@Column(name = "primer_apellido")
	private String primer_apellido;
	@Column(name = "segundo_apellido")
	private String segundo_apellido;
	@Column(name = "fecha_nacimiento")
	private Date fecha_nacimiento;
	@Column(name = "num_colegiado")
	private String num_colegiado;
	@Column(name = "email")
	private String email;
	@Column(name = "id_categoriaprofesional")
	private Integer id_categoriaprofesional;
	@Column(name = "id_centrosanitario")
	private Integer id_centrosanitario;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_medico", cascade = { CascadeType.ALL })
	private List<AnaliticaEntity> lista_analitica = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_medico", cascade = { CascadeType.ALL })
	private List<ProcedimientoEntity> lista_procedimento = new ArrayList<>();
	
	public MedicoEntity() {
		super();
	}

	public MedicoEntity(int id, Integer id_servicio, Integer id_especialidad, String dni, String nombre,
			String primer_apellido, String segundo_apellido, Date fecha_nacimiento, String num_colegiado, String email,
			Integer id_categoriaprofesional, Integer id_centrosanitario) {
		super();
		this.id = id;
		this.id_servicio = id_servicio;
		this.id_especialidad = id_especialidad;
		this.dni = dni;
		this.nombre = nombre;
		this.primer_apellido = primer_apellido;
		this.segundo_apellido = segundo_apellido;
		this.fecha_nacimiento = fecha_nacimiento;
		this.num_colegiado = num_colegiado;
		this.email = email;
		this.id_categoriaprofesional = id_categoriaprofesional;
		this.id_centrosanitario = id_centrosanitario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getId_servicio() {
		return id_servicio;
	}

	public void setId_servicio(Integer id_servicio) {
		this.id_servicio = id_servicio;
	}

	public Integer getId_especialidad() {
		return id_especialidad;
	}

	public void setId_especialidad(Integer id_especialidad) {
		this.id_especialidad = id_especialidad;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimer_apellido() {
		return primer_apellido;
	}

	public void setPrimer_apellido(String primer_apellido) {
		this.primer_apellido = primer_apellido;
	}

	public String getSegundo_apellido() {
		return segundo_apellido;
	}

	public void setSegundo_apellido(String segundo_apellido) {
		this.segundo_apellido = segundo_apellido;
	}

	public Date getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(Date fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}

	public String getNum_colegiado() {
		return num_colegiado;
	}

	public void setNum_colegiado(String num_colegiado) {
		this.num_colegiado = num_colegiado;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getId_categoriaprofesional() {
		return id_categoriaprofesional;
	}

	public void setId_categoriaprofesional(Integer id_categoriaprofesional) {
		this.id_categoriaprofesional = id_categoriaprofesional;
	}

	public Integer getId_centrosanitario() {
		return id_centrosanitario;
	}

	public void setId_centrosanitario(Integer id_centrosanitario) {
		this.id_centrosanitario = id_centrosanitario;
	}

	public Integer getLista_analitica() {
		return lista_analitica.size();
	}

	public void setLista_analitica(List<AnaliticaEntity> lista_analitica) {
		this.lista_analitica = lista_analitica;
	}

	public Integer getLista_procedimento() {
		return lista_procedimento.size();
	}

	public void setLista_procedimento(List<ProcedimientoEntity> lista_procedimento) {
		this.lista_procedimento = lista_procedimento;
	}
	
	

}

package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "episodio")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class EpisodioEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "fecha_firma")
	private Date fecha_firma;
	@Column(name = "fecha_inicio")
	private Date fecha_inicio;
	@Column(name = "fecha_alta")
	private Date fecha_alta;
	@Column(name = "importe")
	private Double importe;
	@Column(name = "finalizado")
	private Boolean finalizado;
	@Column(name = "bloqueado")
	private Boolean bloqueado;
	@Column(name = "id_servicio")
	private Integer id_servicio;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "id_paciente")
	private PacienteEntity id_paciente;
	@Column(name = "id_factura")
	private Integer id_factura;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_dependencia")
	private DependenciaEntity id_dependencia;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_medico")
	private MedicoEntity id_medico;
	@Column(name = "id_tipoepisodio")
	private Integer id_tipoepisodio;
	@Column(name = "id_episodio")
	private Integer id_episodio;
	@Column(name = "id_circunstanciasalta")
	private Integer id_circunstanciasalta;
	@Column(name = "id_destinoalta")
	private Integer id_destinoalta;
	@Column(name = "id_modalidadepisodio")
	private Integer id_modalidadepisodio;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_usuario")
	private UsuarioEntity id_usuario;
	@Column(name = "motivo_ingreso", columnDefinition = "LONGTEXT")
	private String motivo_ingreso;
	@Column(name = "antecedentes", columnDefinition = "LONGTEXT")
	private String antecedentes;
	@Column(name = "historia_actual", columnDefinition = "LONGTEXT")
	private String historia_actual;
	@Column(name = "exploracion_fisica", columnDefinition = "LONGTEXT")
	private String exploracion_fisica;
	@Column(name = "evolucion_comentarios", columnDefinition = "LONGTEXT")
	private String evolucion_comentarios;
	@Column(name = "diagnostico_principal", columnDefinition = "LONGTEXT")
	private String diagnostico_principal;
	@Column(name = "diagnostico_secundarios", columnDefinition = "LONGTEXT")
	private String diagnostico_secundarios;
	@Column(name = "procedimientos", columnDefinition = "LONGTEXT")
	private String procedimientos;
	@Column(name = "tratamiento_recomendaciones", columnDefinition = "LONGTEXT")
	private String tratamiento_recomendaciones;
	@Column(name = "tratamiento_farmacos", columnDefinition = "LONGTEXT")
	private String tratamiento_farmacos;
	@Column(name = "recomendaciones", columnDefinition = "LONGTEXT")
	private String recomendaciones;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_episodio", cascade = { CascadeType.ALL })
	private List<TratamientoEntity> lista_tratamiento = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_episodio", cascade = { CascadeType.ALL })
	private List<AnaliticaEntity> lista_analitica = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_episodio", cascade = { CascadeType.ALL })
	private List<ProcedimientoEntity> lista_procedimiento = new ArrayList<>();	
	
	public EpisodioEntity() {
		super();
	}

	public EpisodioEntity(int id, Date fecha_firma, Date fecha_inicio, Date fecha_alta, Double importe,
			Boolean finalizado, Boolean bloqueado, Integer id_servicio, PacienteEntity id_paciente, Integer id_factura,
			DependenciaEntity id_dependencia, MedicoEntity id_medico, Integer id_tipoepisodio, Integer id_episodio,
			Integer id_circunstanciasalta, Integer id_destinoalta, Integer id_modalidadepisodio,
			UsuarioEntity id_usuario, String motivo_ingreso, String antecedentes, String historia_actual,
			String exploracion_fisica, String evolucion_comentarios, String diagnostico_principal,
			String diagnostico_secundarios, String procedimientos, String tratamiento_recomendaciones,
			String tratamiento_farmacos, String recomendaciones) {
		super();
		this.id = id;
		this.fecha_firma = fecha_firma;
		this.fecha_inicio = fecha_inicio;
		this.fecha_alta = fecha_alta;
		this.importe = importe;
		this.finalizado = finalizado;
		this.bloqueado = bloqueado;
		this.id_servicio = id_servicio;
		this.id_paciente = id_paciente;
		this.id_factura = id_factura;
		this.id_dependencia = id_dependencia;
		this.id_medico = id_medico;
		this.id_tipoepisodio = id_tipoepisodio;
		this.id_episodio = id_episodio;
		this.id_circunstanciasalta = id_circunstanciasalta;
		this.id_destinoalta = id_destinoalta;
		this.id_modalidadepisodio = id_modalidadepisodio;
		this.id_usuario = id_usuario;
		this.motivo_ingreso = motivo_ingreso;
		this.antecedentes = antecedentes;
		this.historia_actual = historia_actual;
		this.exploracion_fisica = exploracion_fisica;
		this.evolucion_comentarios = evolucion_comentarios;
		this.diagnostico_principal = diagnostico_principal;
		this.diagnostico_secundarios = diagnostico_secundarios;
		this.procedimientos = procedimientos;
		this.tratamiento_recomendaciones = tratamiento_recomendaciones;
		this.tratamiento_farmacos = tratamiento_farmacos;
		this.recomendaciones = recomendaciones;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha_firma() {
		return fecha_firma;
	}

	public void setFecha_firma(Date fecha_firma) {
		this.fecha_firma = fecha_firma;
	}

	public Date getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(Date fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public Date getFecha_alta() {
		return fecha_alta;
	}

	public void setFecha_alta(Date fecha_alta) {
		this.fecha_alta = fecha_alta;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Boolean getFinalizado() {
		return finalizado;
	}

	public void setFinalizado(Boolean finalizado) {
		this.finalizado = finalizado;
	}

	public Boolean getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(Boolean bloqueado) {
		this.bloqueado = bloqueado;
	}

	public Integer getId_servicio() {
		return id_servicio;
	}

	public void setId_servicio(Integer id_servicio) {
		this.id_servicio = id_servicio;
	}

	public Integer getId_paciente() {
		return id_paciente.getId();
	}

	public void setId_paciente(PacienteEntity id_paciente) {
		this.id_paciente = id_paciente;
	}

	public Integer getId_factura() {
		return id_factura;
	}

	public void setId_factura(Integer id_factura) {
		this.id_factura = id_factura;
	}

	public DependenciaEntity getId_dependencia() {
		return id_dependencia;
	}

	public void setId_dependencia(DependenciaEntity id_dependencia) {
		this.id_dependencia = id_dependencia;
	}

	public MedicoEntity getId_medico() {
		return id_medico;
	}

	public void setId_medico(MedicoEntity id_medico) {
		this.id_medico = id_medico;
	}

	public Integer getId_tipoepisodio() {
		return id_tipoepisodio;
	}

	public void setId_tipoepisodio(Integer id_tipoepisodio) {
		this.id_tipoepisodio = id_tipoepisodio;
	}

	public Integer getId_episodio() {
		return id_episodio;
	}

	public void setId_episodio(Integer id_episodio) {
		this.id_episodio = id_episodio;
	}

	public Integer getId_circunstanciasalta() {
		return id_circunstanciasalta;
	}

	public void setId_circunstanciasalta(Integer id_circunstanciasalta) {
		this.id_circunstanciasalta = id_circunstanciasalta;
	}

	public Integer getId_destinoalta() {
		return id_destinoalta;
	}

	public void setId_destinoalta(Integer id_destinoalta) {
		this.id_destinoalta = id_destinoalta;
	}

	public Integer getId_modalidadepisodio() {
		return id_modalidadepisodio;
	}

	public void setId_modalidadepisodio(Integer id_modalidadepisodio) {
		this.id_modalidadepisodio = id_modalidadepisodio;
	}

	public Integer getId_usuario() {
		return id_usuario.getId();
	}

	public void setId_usuario(UsuarioEntity id_usuario) {
		this.id_usuario = id_usuario;
	}

	public String getMotivo_ingreso() {
		return motivo_ingreso;
	}

	public void setMotivo_ingreso(String motivo_ingreso) {
		this.motivo_ingreso = motivo_ingreso;
	}

	public String getAntecedentes() {
		return antecedentes;
	}

	public void setAntecedentes(String antecedentes) {
		this.antecedentes = antecedentes;
	}

	public String getHistoria_actual() {
		return historia_actual;
	}

	public void setHistoria_actual(String historia_actual) {
		this.historia_actual = historia_actual;
	}

	public String getExploracion_fisica() {
		return exploracion_fisica;
	}

	public void setExploracion_fisica(String exploracion_fisica) {
		this.exploracion_fisica = exploracion_fisica;
	}

	public String getEvolucion_comentarios() {
		return evolucion_comentarios;
	}

	public void setEvolucion_comentarios(String evolucion_comentarios) {
		this.evolucion_comentarios = evolucion_comentarios;
	}

	public String getDiagnostico_principal() {
		return diagnostico_principal;
	}

	public void setDiagnostico_principal(String diagnostico_principal) {
		this.diagnostico_principal = diagnostico_principal;
	}

	public String getDiagnostico_secundarios() {
		return diagnostico_secundarios;
	}

	public void setDiagnostico_secundarios(String diagnostico_secundarios) {
		this.diagnostico_secundarios = diagnostico_secundarios;
	}

	public String getProcedimientos() {
		return procedimientos;
	}

	public void setProcedimientos(String procedimientos) {
		this.procedimientos = procedimientos;
	}

	public String getTratamiento_recomendaciones() {
		return tratamiento_recomendaciones;
	}

	public void setTratamiento_recomendaciones(String tratamiento_recomendaciones) {
		this.tratamiento_recomendaciones = tratamiento_recomendaciones;
	}

	public String getTratamiento_farmacos() {
		return tratamiento_farmacos;
	}

	public void setTratamiento_farmacos(String tratamiento_farmacos) {
		this.tratamiento_farmacos = tratamiento_farmacos;
	}

	public String getRecomendaciones() {
		return recomendaciones;
	}

	public void setRecomendaciones(String recomendaciones) {
		this.recomendaciones = recomendaciones;
	}

	public List<TratamientoEntity> getLista_tratamiento() {
		return lista_tratamiento;
	}

	public void setLista_tratamiento(List<TratamientoEntity> lista_tratamiento) {
		this.lista_tratamiento = lista_tratamiento;
	}

	public List<AnaliticaEntity> getLista_analitica() {
		return lista_analitica;
	}

	public void setLista_analitica(List<AnaliticaEntity> lista_analitica) {
		this.lista_analitica = lista_analitica;
	}

	public List<ProcedimientoEntity> getLista_procedimiento() {
		return lista_procedimiento;
	}

	public void setLista_procedimiento(List<ProcedimientoEntity> lista_procedimiento) {
		this.lista_procedimiento = lista_procedimiento;
	}

	
}

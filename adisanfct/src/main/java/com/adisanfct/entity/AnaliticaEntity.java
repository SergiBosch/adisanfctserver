package com.adisanfct.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "analitica")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class AnaliticaEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "fecha_prevista")
	private Date fecha_prevista;
	@Column(name = "fecha_realizacion")
	private Date fecha_realizacion;
	@Column(name = "ubicacion")
	private String ubicacion;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_medico")
	private MedicoEntity id_medico;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_dependencia")
	private DependenciaEntity id_dependencia;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_catalogoanaliticas")
	private CatalogoanaliticasEntity id_catalogoanaliticas;
	@Column(name = "importe")
	private Double importe;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_episodio")
	private EpisodioEntity id_episodio;
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "id_usuario")
	private UsuarioEntity id_usuario;
	
	public AnaliticaEntity() {
		super();
	}

	public AnaliticaEntity(int id, String descripcion, Date fecha_prevista, Date fecha_realizacion, String ubicacion,
			MedicoEntity id_medico, DependenciaEntity id_dependencia, CatalogoanaliticasEntity id_catalogoanaliticas, Double importe,
			EpisodioEntity id_episodio) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.fecha_prevista = fecha_prevista;
		this.fecha_realizacion = fecha_realizacion;
		this.ubicacion = ubicacion;
		this.id_medico = id_medico;
		this.id_dependencia = id_dependencia;
		this.id_catalogoanaliticas = id_catalogoanaliticas;
		this.importe = importe;
		this.id_episodio = id_episodio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Date getFecha_prevista() {
		return fecha_prevista;
	}

	public void setFecha_prevista(Date fecha_prevista) {
		this.fecha_prevista = fecha_prevista;
	}

	public Date getFecha_realizacion() {
		return fecha_realizacion;
	}

	public void setFecha_realizacion(Date fecha_realizacion) {
		this.fecha_realizacion = fecha_realizacion;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public MedicoEntity getId_medico() {
		return id_medico;
	}

	public void setId_medico(MedicoEntity id_medico) {
		this.id_medico = id_medico;
	}

	public DependenciaEntity getId_dependencia() {
		return id_dependencia;
	}

	public void setId_dependencia(DependenciaEntity id_dependencia) {
		this.id_dependencia = id_dependencia;
	}

	public CatalogoanaliticasEntity getId_catalogoanaliticas() {
		return id_catalogoanaliticas;
	}

	public void setId_catalogoanaliticas(CatalogoanaliticasEntity id_catalogoanaliticas) {
		this.id_catalogoanaliticas = id_catalogoanaliticas;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Integer getId_episodio() {
		return id_episodio.getId();
	}

	public void setId_episodio(EpisodioEntity id_episodio) {
		this.id_episodio = id_episodio;
	}

	public void setId_usuario(UsuarioEntity id_usuario) {
		this.id_usuario = id_usuario;
	}

	
		
}

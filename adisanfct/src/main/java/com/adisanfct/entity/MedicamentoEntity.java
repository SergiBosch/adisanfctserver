package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "medicamento")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MedicamentoEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "descripcion")
	private String descripcion;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_medicamento", cascade = { CascadeType.ALL })
	private List<TratamientoEntity> lista_tratamiento = new ArrayList<>();
	
	public MedicamentoEntity() {
		super();
	}

	public MedicamentoEntity(int id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public MedicamentoEntity(int id, String descripcion, List<TratamientoEntity> lista_tratamiento) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.lista_tratamiento = lista_tratamiento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getLista_tratamiento() {
		return lista_tratamiento.size();
	}

	public void setLista_tratamiento(List<TratamientoEntity> lista_tratamiento) {
		this.lista_tratamiento = lista_tratamiento;
	}

}

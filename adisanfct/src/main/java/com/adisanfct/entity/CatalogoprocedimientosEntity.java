package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "catalogoprocedimientos")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class CatalogoprocedimientosEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "codigo")
	private String codigo;
	@Column(name = "descripcion", columnDefinition = "LONGTEXT")
	private String descripcion;
	@Column(name = "id_tipoprocedimiento")
	private Integer id_tipoprocedimiento;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_procedimiento", cascade = { CascadeType.ALL })
	private List<ProcedimientoEntity> lista_procedimiento = new ArrayList<>();
	
	public CatalogoprocedimientosEntity() {
		super();
	}

	public CatalogoprocedimientosEntity(int id, String codigo, String descripcion, Integer id_tipoprocedimiento) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.id_tipoprocedimiento = id_tipoprocedimiento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getId_tipoprocedimiento() {
		return id_tipoprocedimiento;
	}

	public void setId_tipoprocedimiento(Integer id_tipoprocedimiento) {
		this.id_tipoprocedimiento = id_tipoprocedimiento;
	}

	public Integer getLista_analitica() {
		return lista_procedimiento.size();
	}

	public void setLista_analitica(List<ProcedimientoEntity> lista_analitica) {
		this.lista_procedimiento = lista_analitica;
	}

	
}

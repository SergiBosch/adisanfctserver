package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "dependencia")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class DependenciaEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "codigo")
	private String codigo;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "id_tipodependencia")
	private Integer id_tipodependencia;
	@Column(name = "id_centrosanitario")
	private Integer id_centrosanitario;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_dependencia", cascade = { CascadeType.ALL })
	private List<AnaliticaEntity> lista_analitica = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_procedimiento", cascade = { CascadeType.ALL })
	private List<ProcedimientoEntity> lista_procedimento = new ArrayList<>();
	
	public DependenciaEntity() {
		super();
	}

	public DependenciaEntity(int id, String codigo, String descripcion, Integer id_tipodependencia,
			Integer id_centrosanitario) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.id_tipodependencia = id_tipodependencia;
		this.id_centrosanitario = id_centrosanitario;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getId_tipodependencia() {
		return id_tipodependencia;
	}

	public void setId_tipodependencia(Integer id_tipodependencia) {
		this.id_tipodependencia = id_tipodependencia;
	}

	public Integer getId_centrosanitario() {
		return id_centrosanitario;
	}

	public void setId_centrosanitario(Integer id_centrosanitario) {
		this.id_centrosanitario = id_centrosanitario;
	}

	public Integer getLista_analitica() {
		return lista_analitica.size();
	}

	public void setLista_analitica(List<AnaliticaEntity> lista_analitica) {
		this.lista_analitica = lista_analitica;
	}

	public Integer getLista_procedimento() {
		return lista_procedimento.size();
	}

	public void setLista_procedimento(List<ProcedimientoEntity> lista_procedimento) {
		this.lista_procedimento = lista_procedimento;
	}
	
	

}

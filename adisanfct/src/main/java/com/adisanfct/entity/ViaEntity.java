package com.adisanfct.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@SuppressWarnings("serial")
@Entity
@Table(name = "via")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ViaEntity implements Serializable, GenericEntityInterface{
		
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "via")
	private String via;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "id_via", cascade = { CascadeType.ALL })
	private List<TratamientoEntity> lista_tratamiento = new ArrayList<>();
	
	public ViaEntity() {
		super();
	}

	public ViaEntity(int id, String via) {
		super();
		this.id = id;
		this.via = via;
	}

	public ViaEntity(int id, String via, List<TratamientoEntity> lista_tratamiento) {
		super();
		this.id = id;
		this.via = via;
		this.lista_tratamiento = lista_tratamiento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVia() {
		return via;
	}

	public void setVia(String via) {
		this.via = via;
	}

	public Integer getLista_tratamiento() {
		return lista_tratamiento.size();
	}

	public void setLista_tratamiento(List<TratamientoEntity> lista_tratamiento) {
		this.lista_tratamiento = lista_tratamiento;
	}

}
